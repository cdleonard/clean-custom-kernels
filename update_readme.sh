#! /bin/bash

base=$(dirname "$0")
{
    "$base/clean-custom-kernels.sh" --help 2>&1
    cat <<MSG
----

This is implemented as a single-file bash script.
MSG
} > "$base/README.rst"
