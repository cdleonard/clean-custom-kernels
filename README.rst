clean-custom-kernels.sh: Remove custom kernels from /boot and /lib/modules
==========================================================================

This wipes kernels other than those shipped by the distro and the running kernel

Options
-------
-h, --help                  Show this message.
-n, --dry-run               Just print commands without executing them.
----

This is implemented as a single-file bash script.
