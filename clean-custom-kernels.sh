#! /bin/bash

set -e

print_help()
{
    cat >&2 <<MSG
clean-custom-kernels.sh: Remove custom kernels from /boot and /lib/modules
==========================================================================

This wipes kernels other than those shipped by the distro and the running kernel

Options
-------
-h, --help                  Show this message.
-n, --dry-run               Just print commands without executing them.
MSG
}

parse_args()
{
    # Defaults
    opt_dry_run=0

    # Scan args
    local opts
    opts=$(getopt -n "$0" -o hn -l "help,dry-run" -- "$@")
    eval set -- "$opts"
    while [ $# -gt 0 ]; do
        case "$1" in
        -h|--help) print_help; exit 1;;
        -n|--dry-run) opt_dry_run=1 ;;
        --)
            if [[ $# != 1 ]]; then
                echo >&2 "ERROR: Unhandled extra arguments: $*"
                exit 1
            fi
            ;;
        esac
        shift
    done
}

list_distro_kver()
{
    dpkg-query --showformat='${Package}\n' --show 'linux-image-*' | sed -ne 's/linux-image-//p'
}

define_dry_func()
{
    if [[ $opt_dry_run == 1 ]]; then
        dry() {
            echo >&2 "DRY:$(printf " %q" "$@")"
        }
    else
        dry() {
            echo >&2 "RUN:$(printf " %q" "$@")"
            "$@"
        }
    fi
}

main()
{
    parse_args "$@"
    define_dry_func

    local -a distro_kver keep_kver
    mapfile -t distro_kver < <(list_distro_kver)
    echo >&2 "INFO: Distro kernels: ${distro_kver[*]}"

    keep_kver=("${distro_kver[@]}" "$(uname -r)")
    should_keep_kver()
    {
        echo "${keep_kver[@]}" | grep -qw "$1"
    }

    local clean_todo=()
    local item kver
    for item in /boot/{vmlinuz,System.map,config,initrd.img}-*; do
        [[ -f $item ]] || continue
        kver=${item#*-}
        if ! should_keep_kver "$kver"; then
            clean_todo+=("$item")
        fi
    done
    for item in /lib/modules/*; do
        [[ -d $item ]] || continue
        kver=$(basename "$item")
        if ! should_keep_kver "$kver"; then
            clean_todo+=("$item")
        fi
    done

    if [[ ${#clean_todo[@]} == 0 ]]; then
        echo >&2 "INFO: Nothing to do"
    else
        dry rm -rf "${clean_todo[@]}"
    fi
}

main "$@"
